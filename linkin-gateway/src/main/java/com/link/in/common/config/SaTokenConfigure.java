package com.link.in.common.config;

import cn.dev33.satoken.config.SaTokenConfig;
import cn.dev33.satoken.context.SaHolder;
import cn.dev33.satoken.reactor.filter.SaReactorFilter;
import cn.dev33.satoken.router.SaHttpMethod;
import cn.dev33.satoken.router.SaRouter;
import cn.dev33.satoken.stp.StpUtil;
import cn.dev33.satoken.util.SaResult;
import com.link.in.common.st.util.StpSysUtil;
import com.link.in.common.st.util.StpUserUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;

/**
 * [Sa-Token 权限认证] 配置类
 * @author kong
 */
@Slf4j
@Configuration
public class SaTokenConfigure {
    // 注册 Sa-Token全局过滤器
    @Bean
    @Primary
    public SaReactorFilter getSaReactorFilter() {
        return new SaReactorFilter()
                // 拦截地址
                .addInclude("/**")    /* 拦截全部path */
                // 开放地址
                .addExclude("/favicon.ico")
                .addExclude("/linkin-auth/user/login")
                .addExclude("/linkin-auth/manager/login")
                // 鉴权方法：每次访问进入
                .setAuth(obj -> {
                    System.out.println("---------- sa全局认证");
                    System.out.println(StpUserUtil.isLogin());
                    System.out.println(StpSysUtil.isLogin());

                    // /user 下，除login 接口外，都要验证是否登录
                    SaRouter.match("/linkin-auth/user/**").check(r -> StpUserUtil.checkLogin());

                    // /manager 下，除login 接口外，都要验证是否登录
                    SaRouter.match("/linkin-auth/manager/**").check(r -> StpSysUtil.checkLogin());

                    // 访问移动端的接口，需要StpUserUtil登录过
                    SaRouter.match("/linkin-mobile/**", r -> StpUserUtil.checkLogin());

                    // 访问管理员后台，需要StpSysUtil登录过
                    SaRouter.match("/linkin-sys/**", r -> StpSysUtil.checkLogin());
                })
                // 异常处理方法：每次setAuth函数出现异常时进入
                .setError(e -> {
                    log.error(e.getMessage(), e);
                    return SaResult.error(e.getMessage());
                })
                .setBeforeAuth(obj -> {
                    // ---------- 设置跨域响应头 ----------
                    SaHolder.getResponse()
                            // 允许指定域访问跨域资源
                            .setHeader("Access-Control-Allow-Origin", "*")
                            // 允许所有请求方式
                            .setHeader("Access-Control-Allow-Methods", "POST, GET, OPTIONS, DELETE")
                            // 有效时间
                            .setHeader("Access-Control-Max-Age", "3600")
                            // 允许的header参数
                            .setHeader("Access-Control-Allow-Headers", "*");

                    // 如果是预检请求，则立即返回到前端
                    SaRouter.match(SaHttpMethod.OPTIONS)
                            .free(r -> System.out.println("--------OPTIONS预检请求，不做处理"))
                            .back();
                });
    }
}
