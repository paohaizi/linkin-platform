

## linkin-platform

### 简介

采用Satoken 做鉴权框架的 简单版 微服务

### 前端

- 请前往 https://gitee.com/paohaizi/linkin-elementplus.git

### 后端涉及技术

- SpringCloud
- Nacos （请自行操作，建议开发环境用docker跑）
- SpringBoot
- Mybatis-Plus
- Redis
- MySql
- SaToken 1.37.0

### 模块级别

- linkin-platform
  
  - linkin-auth
  
  - linkin-common
  - linkin-gateway
  - linkin-mobile
  - linkin-redis
  - linkin-system
  - linkin-tool

### linkin-auth 登录模块

> 在inter包下，有调用其他 服务的 示例代码。

### linkin-common 公共

> 在公共包内，有 StpUserUtil  和  StpSysUtil  两个类，请参考 Satoken 多账号认证：
>
> https://sa-token.cc/doc.html#/up/many-account
>
> 目的是为了区分不同端的用户登录

- StpUserUtil
  - 普通用户登录：例如 移动端，小程序，H5等面向外部的C端用户。
- StpSysUtil
  - 后台系统用户登录：后台管理员 之类的。

### linkin-gateway 网关

- SaTokenConfigure 类

  > 请求拦截配置，可改为白名单开放地址表

### linkin-mobile 移动端

### linkin-redis  缓存

- 采用SPI的方式，注入到 父引用方

### linkin-system 后台系统

> SaSysCheckPermission 类是system 模块用来判断权限的，由于Satoken默认的 权限判断使用的是 **SaCheckPermission** ，所以必须另外写一个用来判断 StpSysUtil 的。

### linkin-tool 生成代码工具

### 参考及使用的技术

- SaToken

[SaToken]: https://sa-token.dev33.cn/doc/index.html#/