package com.link.in.auth.service;

import com.link.in.login.model.SysUser;

public interface SysUserService {
    SysUser findByUserName(String username);
}
