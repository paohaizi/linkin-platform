package com.link.in.auth.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.link.in.login.model.SysUser;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

/**
 * @author Ujun
 * @date 2023/10/1
 * @apiNote
 */
public interface SysUserMapper extends BaseMapper<SysUser> {
}
