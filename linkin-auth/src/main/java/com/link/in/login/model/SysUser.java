package com.link.in.login.model;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import lombok.Data;

import java.io.Serializable;

/**
 * @author Ujun
 * @date 2023/10/1
 * @apiNote
 */
@Data
@TableName("sys_user")
public class SysUser extends Model<SysUser> implements Serializable {
    private static final long serialVersionUID = -5886012896705137070L;
    @TableId(value="id",type= IdType.ASSIGN_ID)  //雪花算法  id生成策略
    @JsonSerialize(using= ToStringSerializer.class)
    private Long id;
    private String username;
    private String password;
}
