package com.link.in.login.controller;

import cn.dev33.satoken.stp.SaTokenInfo;
import cn.dev33.satoken.stp.StpUtil;
import cn.dev33.satoken.util.SaResult;
import com.link.in.common.st.util.StpUserUtil;
import com.link.in.inter.SpCfgInterface;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/user")
public class UserLoginController {
    @Autowired
    SpCfgInterface spCfgInterface;

    /**
     * 普通用户登录
     * @return
     */
    @GetMapping("/login")
    public SaResult login() {
        StpUserUtil.login("user1");
        SaTokenInfo saTokenInfo = StpUserUtil.getTokenInfo();
        return SaResult.data(saTokenInfo);
    }

    /**
     * 获取用户信息
     * @return
     */
    @GetMapping("/getUser")
    public SaResult getUser() {
        String userId = StpUserUtil.getLoginIdAsString();
        String test = spCfgInterface.getInfo();
        System.out.println("test:" + test);
        return SaResult.data(userId);
    }

}
