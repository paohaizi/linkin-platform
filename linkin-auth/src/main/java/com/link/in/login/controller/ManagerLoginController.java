package com.link.in.login.controller;

import cn.dev33.satoken.stp.SaTokenInfo;
import com.link.in.auth.service.SysUserService;
import com.link.in.common.st.util.StpSysUtil;
import com.link.in.common.util.MD5Util;
import com.link.in.common.util.R;
import com.link.in.login.bean.LoginBean;
import com.link.in.login.model.SysUser;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@Slf4j
@RestController
@RequestMapping("/manager")
public class ManagerLoginController {
    @Autowired
    private SysUserService sysUserService;

    /**
     * 普通用户登录
     * @return
     */
    @PostMapping("/login")
    public R login(@Valid @RequestBody LoginBean loginBean) {
        String username = loginBean.getUsername();
        SysUser sysUser = sysUserService.findByUserName(username);
        if (sysUser == null) {
            return R.error
                    ("用户名或密码错误");
        }
        String password = loginBean.getPassword();

        // 得到加密后的密码
        String pwd = sysUser.getPassword();
        if (!MD5Util.verify(password, pwd)) {
            return R.error("用户名或密码错误");
        } else {
            // 验证用户名和密码通过
            long id = sysUser.getId();
            StpSysUtil.login(id);
            SaTokenInfo saTokenInfo = StpSysUtil.getTokenInfo();
            if (saTokenInfo == null) {
                return R.error("登录失败, 请重新登录");
            }
            saTokenInfo.setLoginDevice(null);
            // saTokenInfo.setTokenName(null);
            return R.data(saTokenInfo);
        }
    }

    @PostMapping("/logout")
    public R logout() {
        log.info("退出登录--------------");
        StpSysUtil.logout();
        return R.success();
    }

    /**
     * 获取用户信息
     * @return
     */
    @GetMapping("/info")
    public R getUser() {
        String userId = StpSysUtil.getLoginIdAsString();
        return R.data(userId);
    }
}
