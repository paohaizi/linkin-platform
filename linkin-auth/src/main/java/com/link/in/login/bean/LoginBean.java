package com.link.in.login.bean;

import lombok.Data;
import lombok.ToString;

import javax.validation.constraints.NotBlank;

/**
 * @author Ujun
 * @date 2023/10/1
 * @apiNote
 */
@Data
@ToString
public class LoginBean {
    @NotBlank(message = "用户名必须填写")
    private String username;

    @NotBlank(message = "密码必须填写")
    private String password;
}
