package com.link.in.inter;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * 服务调用
 */
@FeignClient(
        name = "linkin-sys",                 // 服务名称
        configuration = FeignInterceptor.class        // 请求拦截器 （关键代码）
)
public interface SpCfgInterface {

    // 获取server端指定配置信息
    @RequestMapping("/test/getInfo")
    public String getInfo();

}
