package com.link.in.inter;

import cn.dev33.satoken.same.SaSameUtil;
import feign.RequestInterceptor;
import org.springframework.stereotype.Component;

@Component
public class FeignInterceptor implements RequestInterceptor {
    // 为 Feign 的 RCP调用 添加请求头Same-Token

    @Override
    public void apply(feign.RequestTemplate requestTemplate) {
        requestTemplate.header(SaSameUtil.SAME_TOKEN, SaSameUtil.getToken());
    }
}
