package com.link.in.sys.controller;


import cn.dev33.satoken.annotation.SaCheckPermission;
import cn.dev33.satoken.util.SaResult;
import com.link.in.common.exception.service.ServiceException;
import com.link.in.common.util.R;
import com.link.in.sys.bean.ReqMenuBean;
import com.link.in.sys.bean.ReqRoleMenu;
import com.link.in.sys.bean.RspMenuTree;
import com.link.in.sys.bean.RspMenuTreeRole;
import com.link.in.sys.entity.SysMenu;
import com.link.in.sys.entity.SysRoleMenu;
import com.link.in.sys.service.SysMenuService;
import com.link.in.sys.service.SysRoleMenuService;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.*;
import java.util.stream.Collectors;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author Ujun
 * @since 2024-03-16
 */
@Slf4j
@RestController
@RequestMapping("/sys/menu")
public class SysMenuController {
    @Autowired
    private SysMenuService sysMenuService;
    @Autowired
    private SysRoleMenuService sysRoleMenuService;

    /**
     * 查询用户自己的菜单
     * @return
     */
    @GetMapping("/user")
    public R user() {
        List<RspMenuTree> list = sysMenuService.findUserMenu();
        return R.data(list);
    }


    @GetMapping("/treeMenu")
    public R treeMenu() {
        List<RspMenuTreeRole> menus = sysMenuService.findAll();
        return R.data(menus);
    }

    @PostMapping("/granted")
    public R granted(@Valid @RequestBody ReqRoleMenu reqRoleMenu) {
        sysMenuService.granted(reqRoleMenu.getRoleId(), reqRoleMenu.getMenuIds());
        return R.success("操作成功");
    }

    @PostMapping("saveOrUpdate")
    public R saveOrUpdate(@Valid @RequestBody ReqMenuBean reqMenuBean) {
        return sysMenuService.saveBean(reqMenuBean) ? R.success("操作成功") : R.error("操作失败");
    }

    @PostMapping("/delete/{id}")
    public R deleteById(@PathVariable Integer id) {
        boolean rm = sysMenuService.deleteSubTreeById(id);
        return rm ? R.success("操作成功") : R.error("操作失败");
    }

    @GetMapping("/role/{roleId}")
    public R findMenusByRoleId(@PathVariable Integer roleId) {
        List<SysRoleMenu> roleMenus = sysRoleMenuService.findByRoleId(roleId); // 获取该角色对应的菜单
        List<Integer> menuIds = roleMenus.stream().map(SysRoleMenu::getMenuId)
                .collect(Collectors.toList());
        return R.data(menuIds);
    }
}

