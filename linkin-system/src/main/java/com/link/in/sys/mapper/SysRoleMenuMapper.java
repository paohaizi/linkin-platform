package com.link.in.sys.mapper;

import com.link.in.sys.entity.SysRoleMenu;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author Ujun
 * @since 2024-03-16
 */
public interface SysRoleMenuMapper extends BaseMapper<SysRoleMenu> {
}
