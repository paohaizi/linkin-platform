package com.link.in.sys.bean;

import lombok.Data;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

/**
 * @author Ujun
 * @date 2024/3/18
 * @apiNote
 */
@Data
public class ReqUserBean {
    private Long id;

    @NotNull
    private String username;

    @NotNull
    private String nickName;

    private String phone;

    private String email;

    @Min(value = 0, message = "请选中角色")
    private Integer roleId;
}
