package com.link.in.sys.service;

import cn.dev33.satoken.util.SaResult;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.link.in.common.enums.ResultCodeEnum;
import com.link.in.sys.bean.ReqPageBean;
import com.link.in.sys.bean.ReqUserBean;
import com.link.in.sys.bean.RspLoginUser;
import com.link.in.sys.entity.SysUser;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.Map;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author Ujun
 * @since 2024-03-16
 */
public interface SysUserService extends IService<SysUser> {

    RspLoginUser findUser();

    IPage<SysUser> pageList(ReqPageBean reqPageBean);

    boolean updateStatus(Long id, int status);

    boolean saveBean(ReqUserBean reqUserBean);

    ResultCodeEnum updatePassword(Long id, String oldPassword, String newPassword);
}
