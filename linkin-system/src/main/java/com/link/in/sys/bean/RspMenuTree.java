package com.link.in.sys.bean;

import com.baomidou.mybatisplus.annotation.TableField;
import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import com.link.in.sys.entity.SysMenu;
import lombok.Builder;
import lombok.Data;
import org.apache.commons.lang3.StringUtils;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * @author Ujun
 * @date 2024/3/16
 * @apiNote
 */
@Data
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id")
public class RspMenuTree implements Serializable {

    private Integer id;

    private Integer parentId;

    /**
     * 标识名称
     */
    private String name;

    private String path;

    /**
     * 组件
     */
    private String component;

    private String redirect;

    private Meta meta;

    /**
     * 排序
     */
    private Integer sort;

    private List<RspMenuTree> children;

    @Data
    @Builder
    private static class Meta {
        private Title title;

        private String icon;
    }

    @Data
    @Builder
    private static class Title {
        private String zh_CN;

        private String en_US;

        public Title(String zh_CN, String en_US) {
            this.zh_CN = zh_CN;
            this.en_US = en_US;
        }
    }

    public RspMenuTree() {}

    public RspMenuTree(SysMenu sysMenu) {
        this.id = sysMenu.getId();
        this.parentId = sysMenu.getParentId();
        this.path = sysMenu.getPath();
        this.name = sysMenu.getName();
        this.component = sysMenu.getComponent();
        this.redirect = sysMenu.getRedirect();

        Title title = RspMenuTree.Title.builder()
                .zh_CN(sysMenu.getTitleZh())
                .en_US(sysMenu.getTitleUs())
                .build();

        this.meta = RspMenuTree.Meta.builder()
                .title(title)
                .icon(StringUtils.isNotBlank(sysMenu.getIcon()) ? sysMenu.getIcon() : null)
                .build();
    }
}
