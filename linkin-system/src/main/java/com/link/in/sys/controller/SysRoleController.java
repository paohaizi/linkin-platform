package com.link.in.sys.controller;


import cn.dev33.satoken.annotation.SaCheckPermission;
import cn.dev33.satoken.util.SaResult;
import com.link.in.common.exception.service.ServiceException;
import com.link.in.common.util.R;
import com.link.in.sys.bean.ReqRoleBean;
import com.link.in.sys.entity.SysRole;
import com.link.in.sys.service.SysRoleService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.Map;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author Ujun
 * @since 2024-03-16
 */
@Slf4j
@RestController
@RequestMapping("/sys/role")
public class SysRoleController {
    @Autowired
    SysRoleService sysRoleService;

    @GetMapping("/findAll")
    public R findAll(@RequestParam Map<String, Object> paramMap) {
        List<SysRole> list = sysRoleService.findAll(paramMap);
        return R.data(list);
    }

    @PostMapping("/saveBean")
    public R saveBean(@Valid @RequestBody ReqRoleBean reqRoleBean){
        boolean isSave = sysRoleService.saveBean(reqRoleBean);
        return isSave ? R.success() : R.error("失败");
    }
}

