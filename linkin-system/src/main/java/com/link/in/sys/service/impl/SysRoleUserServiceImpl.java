package com.link.in.sys.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.link.in.sys.entity.SysRole;
import com.link.in.sys.entity.SysRoleUser;
import com.link.in.sys.mapper.SysRoleUserMapper;
import com.link.in.sys.service.SysRoleUserService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author Ujun
 * @since 2024-03-16
 */
@Service
public class SysRoleUserServiceImpl extends ServiceImpl<SysRoleUserMapper, SysRoleUser> implements SysRoleUserService {

    @Override
    public SysRole findRoleByUserId(Long userId) {
        return this.baseMapper.findRoleByUserId(userId);
    }

    @Override
    public boolean deleteByUserId(Long userId) {
        QueryWrapper<SysRoleUser> wrapper = new QueryWrapper<>();
        wrapper.eq("user_id", userId);
        return remove(wrapper);
    }

    @Override
    public boolean saveBean(Long userId, Integer roleId) {
        SysRoleUser roleUser = new SysRoleUser();
        roleUser.setUserId(userId);
        roleUser.setRoleId(roleId);
        return save(roleUser);
    }
}
