package com.link.in.sys.bean;

import lombok.Data;

import javax.validation.constraints.Min;

/**
 * @author Ujun
 * @date 2024/3/16
 * @apiNote
 */
@Data
public class ReqPageBean {
    @Min(value = 0, message = "当前页码不能小于0")
    private int page;

    @Min(value = 1, message = "每页条数不能小于1")
    private int size;
}
