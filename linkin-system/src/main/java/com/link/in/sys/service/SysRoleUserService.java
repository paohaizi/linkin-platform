package com.link.in.sys.service;

import com.link.in.sys.entity.SysRole;
import com.link.in.sys.entity.SysRoleUser;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author Ujun
 * @since 2024-03-16
 */
public interface SysRoleUserService extends IService<SysRoleUser> {

    SysRole findRoleByUserId(Long userId);

    boolean deleteByUserId(Long id);

    boolean saveBean(Long id, Integer roleId);
}
