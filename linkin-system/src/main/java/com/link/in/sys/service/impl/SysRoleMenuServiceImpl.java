package com.link.in.sys.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.link.in.sys.entity.SysMenu;
import com.link.in.sys.entity.SysRoleMenu;
import com.link.in.sys.mapper.SysRoleMenuMapper;
import com.link.in.sys.service.SysRoleMenuService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author Ujun
 * @since 2024-03-16
 */
@Service
public class SysRoleMenuServiceImpl extends ServiceImpl<SysRoleMenuMapper, SysRoleMenu> implements SysRoleMenuService {

    @Override
    public boolean deleteByRoleId(Integer roleId, Integer menuId) {
        QueryWrapper<SysRoleMenu> wrapper = new QueryWrapper<>();
        wrapper.eq("role_id", roleId);
        return this.remove(wrapper);
    }

    @Override
    public List<SysRoleMenu> findByRoleId(Integer roleId) {
        QueryWrapper<SysRoleMenu> wrapper = new QueryWrapper<>();
        wrapper.eq("role_id", roleId);
        return list(wrapper);
    }
}
