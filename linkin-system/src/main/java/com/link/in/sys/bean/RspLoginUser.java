package com.link.in.sys.bean;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 * @author Ujun
 * @date 2024/3/16
 * @apiNote
 */
@Data
public class RspLoginUser implements Serializable {
    @JsonFormat(shape = JsonFormat.Shape.STRING)
    private Long id;
    private String nickName;
    private String avatar;
    private String role;
    private List<String> sysPermissions;
}
