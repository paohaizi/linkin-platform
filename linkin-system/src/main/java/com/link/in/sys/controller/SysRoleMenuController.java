package com.link.in.sys.controller;


import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author Ujun
 * @since 2024-03-16
 */
@RestController
@RequestMapping("/sys/sys-role-menu")
public class SysRoleMenuController {

}

