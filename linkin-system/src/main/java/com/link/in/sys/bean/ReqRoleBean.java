package com.link.in.sys.bean;

import lombok.Data;

/**
 * @author Ujun
 * @date 2024/3/18
 * @apiNote
 */
@Data
public class ReqRoleBean {
    private Long id;

    private String code;

    private String name;
}
