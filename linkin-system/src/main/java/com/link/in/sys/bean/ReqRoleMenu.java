package com.link.in.sys.bean;

import lombok.Data;

import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * @author Ujun
 * @date 2024/3/23
 * @apiNote
 */
@Data
public class ReqRoleMenu {
    @NotNull
    private Integer roleId;

    @NotNull
    private List<Integer> menuIds;
}
