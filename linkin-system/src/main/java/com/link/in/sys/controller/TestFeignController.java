package com.link.in.sys.controller;

import cn.dev33.satoken.util.SaResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/test")
public class TestFeignController {

    @GetMapping("/getInfo")
    public String getInfo() {
        String user = "MgrAdmin";
        return user;
    }
}
