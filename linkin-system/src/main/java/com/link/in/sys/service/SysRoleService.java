package com.link.in.sys.service;

import cn.dev33.satoken.util.SaResult;
import com.link.in.sys.bean.ReqRoleBean;
import com.link.in.sys.entity.SysRole;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;
import java.util.Map;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author Ujun
 * @since 2024-03-16
 */
public interface SysRoleService extends IService<SysRole> {

    boolean saveBean(ReqRoleBean reqRoleBean);

    List<SysRole> findAll(Map<String, Object> paramMap);
}
