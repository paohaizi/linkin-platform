package com.link.in.sys.bean;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.Data;

import java.io.Serializable;

/**
 * @author Ujun
 * @date 2024/3/24
 * @apiNote
 */
@Data
public class ReqMenuBean implements Serializable {
    private static final long serialVersionUID=1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    @TableField("parent_Id")
    private Integer parentId;

    /**
     * 标识名称
     */
    private String name;

    private String path;

    /**
     * 组件
     */
    private String component;

    private String redirect;

    /**
     * 显示标题
     */
    private String titleZh;

    private String titleUs;

    /**
     * 图标
     */
    private String icon;

    /**
     * 0目录 1菜单 2按钮
     */
    private Integer level;

    /**
     * 排序
     */
    private Integer sort;


    /**
     * 是否菜单 1 是 2 不是
     */
    private Integer isMenu;

    /**
     * 按钮权限
     */
    private String permission;
}
