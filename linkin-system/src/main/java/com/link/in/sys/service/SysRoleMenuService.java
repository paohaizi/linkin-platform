package com.link.in.sys.service;

import com.link.in.sys.entity.SysMenu;
import com.link.in.sys.entity.SysRoleMenu;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author Ujun
 * @since 2024-03-16
 */
public interface SysRoleMenuService extends IService<SysRoleMenu> {

    boolean deleteByRoleId(Integer roleId, Integer o);

    List<SysRoleMenu> findByRoleId(Integer roleId);
}
