package com.link.in.sys.service.impl;

import cn.dev33.satoken.util.SaResult;
import com.link.in.common.exception.service.ServiceException;
import com.link.in.common.st.util.StpSysUtil;
import com.link.in.common.util.TreeUtil;
import com.link.in.sys.bean.ReqMenuBean;
import com.link.in.sys.bean.RspMenuTree;
import com.link.in.sys.bean.RspMenuTreeRole;
import com.link.in.sys.entity.SysMenu;
import com.link.in.sys.entity.SysRole;
import com.link.in.sys.entity.SysRoleMenu;
import com.link.in.sys.mapper.SysMenuMapper;
import com.link.in.sys.mapper.SysRoleMenuMapper;
import com.link.in.sys.service.SysMenuService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.link.in.sys.service.SysRoleMenuService;
import com.link.in.sys.service.SysRoleUserService;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.ObjectUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import java.util.*;
import java.util.stream.Collectors;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author Ujun
 * @since 2024-03-16
 */
@Slf4j
@Service
public class SysMenuServiceImpl extends ServiceImpl<SysMenuMapper, SysMenu> implements SysMenuService {
    @Autowired
    private SysRoleUserService sysRoleUserService;
    @Autowired
    private SysRoleMenuService sysRoleMenuService;

    @Override
    public List<RspMenuTree> findUserMenu() {
        Long userId = StpSysUtil.getLoginIdAsLong();
        SysRole role = sysRoleUserService.findRoleByUserId(userId);
        if (role == null) {
            throw new ServiceException("权限不足");
        }
        List<SysMenu> menus = this.baseMapper.findByRoleId(role.getId());

        Optional.ofNullable(menus)
                .filter(menuList -> !menuList.isEmpty())
                .orElseThrow(() -> new ServiceException("未配置菜单"));

        List<RspMenuTree> list = menus.stream()
                .map(RspMenuTree::new)
                .collect(Collectors.toList());
        list = treeBuilder(list);
        return list;
    }

    @Override
    public List<SysMenu> findPermissionByRoleId(Integer id) {
        return this.baseMapper.findPermissionByRoleId(id);
    }

    @Override
    public List<RspMenuTreeRole> findAll() {
        List<SysMenu> menus = list();

        List<RspMenuTreeRole> sysMenuTrees = new ArrayList<>(menus.size());
        RspMenuTreeRole sysMenuTree = null;
        for(SysMenu menu : menus){
            sysMenuTree = new RspMenuTreeRole();
            BeanUtils.copyProperties(menu, sysMenuTree);

            sysMenuTree.setLabel(StringUtils.isBlank(menu.getTitleZh())
                    ? menu.getName() : menu.getTitleZh());

            sysMenuTrees.add(sysMenuTree);
        }
        List<RspMenuTreeRole> sysMenus = TreeUtil.buildTreeData(sysMenuTrees);
        return sysMenus;
    }

    @Override
    @Transactional
    public boolean granted(Integer roleId, List<Integer> menuIds) {
        try {
            sysRoleMenuService.deleteByRoleId(roleId, null);
            if (!CollectionUtils.isEmpty(menuIds)) {
                List<SysRoleMenu> list = menuIds.stream()
                    .map(menuId -> {
                        SysRoleMenu roleMenu = new SysRoleMenu();
                        roleMenu.setMenuId(menuId);
                        roleMenu.setRoleId(roleId);
                        return roleMenu;
                    })
                    .collect(Collectors.toList());
                sysRoleMenuService.saveBatch(list);
            }
        } catch (Exception e) {
            throw new ServiceException(e);
        }
        return true;
    }

    @Override
    public boolean deleteSubTreeById(Integer id) {
        int rm = this.baseMapper.deleteSubTreeById(id);
        return rm > 0;
    }

    @Override
    public boolean saveBean(ReqMenuBean reqMenuBean) {
        SysMenu menu = new SysMenu();
        BeanUtils.copyProperties(reqMenuBean, menu);
        if (menu.getId() == null || menu.getId() == 0) {
            menu.setCreateTime(new Date());
        }
        menu.setUpdateTime(new Date());
        return saveOrUpdate(menu);
    }

    public static List<RspMenuTree> treeBuilder(List<RspMenuTree> menuTrees) {
        List<RspMenuTree> menus = new ArrayList<>();
        try {
            Map<Integer, List<RspMenuTree>> map = new HashMap<>();
            for (RspMenuTree menuTree : menuTrees) {
                Integer parentId = menuTree.getParentId();
                if (parentId == null || parentId == -1) {
                    menus.add(menuTree);
                }
                map.computeIfAbsent(parentId, k -> new ArrayList<>()).add(menuTree);
            }
            for (RspMenuTree menuTree : menuTrees) {
                Integer id = menuTree.getId();
                if (map.containsKey(id)) {
                    menuTree.setChildren(map.get(id));
                }
            }
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            throw new ServiceException(e);
        }
        return menus;
    }
}
