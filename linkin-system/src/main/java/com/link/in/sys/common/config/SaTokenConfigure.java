package com.link.in.sys.common.config;

import cn.dev33.satoken.context.SaHolder;
import cn.dev33.satoken.filter.SaServletFilter;
import cn.dev33.satoken.interceptor.SaInterceptor;
import cn.dev33.satoken.same.SaSameUtil;
import cn.dev33.satoken.strategy.SaStrategy;
import cn.dev33.satoken.util.SaResult;
import com.link.in.common.st.util.StpSysUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.annotation.AnnotatedElementUtils;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@Slf4j
@Configuration
public class SaTokenConfigure implements WebMvcConfigurer {
    String[] freePath = {
            "/mgr-login",   // 管理员登录后台
            "/user/login",  // 普通用户登录
            "/cpy/login"    // 企业登录
    };
    /*@Override
    public void addInterceptors(InterceptorRegistry registry) {
        registry.addInterceptor(new SaInterceptor(handle -> {
            // 如果这个接口，要求客户端登录了后台 Admin 账号才能访问：
            *//*SaRouter.match("/art/getInfo").check(r -> StpUtil.checkLogin());

            // 如果这个接口，要求客户端登录了前台 User 账号才能访问：
            SaRouter.match("/art/getInfo").check(r -> StpUserUtil.checkLogin());

            // 如果这个接口，要求客户端同时登录 Admin 和 User 账号，才能访问：
            SaRouter.match("/art/getInfo").check(r -> {
                StpUtil.checkLogin();
                StpUserUtil.checkLogin();
            });

            // 如果这个接口，要求客户端登录 Admin 和 User 账号任意一个，就能访问：
            SaRouter.match("/art/getInfo").check(r -> {
                if(!StpUtil.isLogin() && !StpUserUtil.isLogin()) {
                    throw new SaTokenException("请登录后再访问接口");
                }
            });*//*

        })).addPathPatterns("/**").excludePathPatterns(freePath);
    }*/

    @Autowired
    public void rewriteSaStrategy() {
        // 重写Sa-Token的注解处理器，增加注解合并功能
        SaStrategy.instance.getAnnotation = (element, annotationClass) -> {
            return AnnotatedElementUtils.getMergedAnnotation(element, annotationClass);
        };
    }

    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        // 注册 Sa-Token 拦截器，打开注解式鉴权功能
        registry.addInterceptor(new SaInterceptor())
                .addPathPatterns("/**");
    }
    @Bean
    public SaServletFilter getSaServletFilter() {
        return new SaServletFilter()
                .addInclude("/**")
                .addExclude("/favicon.ico")
                .setAuth(obj -> {
                    // 校验 Same-Token 身份凭证     —— 以下两句代码可简化为：SaSameUtil.checkCurrentRequestToken();

                    log.info("校验----");
                    String token = SaHolder.getRequest().getHeader(SaSameUtil.SAME_TOKEN);
                    SaSameUtil.checkToken(token);
                })
                .setError(e -> {
                    return SaResult.error(e.getMessage());
                })
                ;
    }
}
