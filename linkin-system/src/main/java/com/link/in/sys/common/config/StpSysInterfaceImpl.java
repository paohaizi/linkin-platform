package com.link.in.sys.common.config;

import cn.dev33.satoken.stp.StpInterface;
import cn.dev33.satoken.stp.StpUtil;
import com.link.in.sys.entity.SysMenu;
import com.link.in.sys.entity.SysRole;
import com.link.in.sys.mapper.SysRoleMenuMapper;
import com.link.in.sys.service.SysMenuService;
import com.link.in.sys.service.SysRoleMenuService;
import com.link.in.sys.service.SysRoleUserService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * @author Ujun
 * @date 2024/3/24
 * @apiNote
 */
@Slf4j
@Component
public class StpSysInterfaceImpl implements StpInterface {
    @Autowired
    SysRoleUserService sysRoleUserService;
    @Autowired
    SysMenuService sysMenuService;


    @Override
    public List<String> getPermissionList(Object loginId, String loginType) {
        Long userId = Long.valueOf(loginId + "");
        SysRole sysRole = sysRoleUserService.findRoleByUserId(userId);
        List<String> permissions = null;
        if (sysRole != null) {
            Integer roleId = sysRole.getId();
            permissions = sysMenuService.findPermissionByRoleId(roleId)
                    .stream()
                    .map(SysMenu::getPermission)
                    .collect(Collectors.toList());
        }
        return permissions;
    }

    @Override
    public List<String> getRoleList(Object loginId, String loginType) {
        // 返回此 loginId 拥有的角色列表
        Long userId = Long.valueOf(loginId + "");

        SysRole sysRole = sysRoleUserService.findRoleByUserId(userId);
        List<String> roles = new ArrayList<>();
        roles.add(sysRole.getCode());
        return roles;
    }
}
