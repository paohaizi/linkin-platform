package com.link.in.sys.service;

import com.link.in.sys.bean.ReqMenuBean;
import com.link.in.sys.bean.RspMenuTree;
import com.link.in.sys.bean.RspMenuTreeRole;
import com.link.in.sys.entity.SysMenu;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author Ujun
 * @since 2024-03-16
 */
public interface SysMenuService extends IService<SysMenu> {

    List<RspMenuTree> findUserMenu();

    List<SysMenu> findPermissionByRoleId(Integer id);

    List<RspMenuTreeRole> findAll();

    boolean granted(Integer roleId, List<Integer> menuIds);

    boolean deleteSubTreeById(Integer id);

    boolean saveBean(ReqMenuBean reqMenuBean);
}
