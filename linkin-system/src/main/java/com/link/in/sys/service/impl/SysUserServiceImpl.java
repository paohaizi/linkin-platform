package com.link.in.sys.service.impl;

import cn.dev33.satoken.stp.StpUtil;
import cn.dev33.satoken.util.SaResult;
import com.alibaba.nacos.common.utils.Md5Utils;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.link.in.common.enums.ResultCodeEnum;
import com.link.in.common.exception.service.ServiceException;
import com.link.in.common.st.util.StpSysUtil;
import com.link.in.common.util.MD5Util;
import com.link.in.common.util.PageUtil;
import com.link.in.sys.bean.ReqPageBean;
import com.link.in.sys.bean.ReqUserBean;
import com.link.in.sys.bean.RspLoginUser;
import com.link.in.sys.entity.SysMenu;
import com.link.in.sys.entity.SysRole;
import com.link.in.sys.entity.SysRoleUser;
import com.link.in.sys.entity.SysUser;
import com.link.in.sys.mapper.SysUserMapper;
import com.link.in.sys.service.SysMenuService;
import com.link.in.sys.service.SysRoleUserService;
import com.link.in.sys.service.SysUserService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;
import java.util.stream.Collectors;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author Ujun
 * @since 2024-03-16
 */
@Slf4j
@Service
public class SysUserServiceImpl extends ServiceImpl<SysUserMapper, SysUser> implements SysUserService {
    @Autowired
    private SysRoleUserService sysRoleUserService;
    @Autowired
    private SysMenuService sysMenuService;
    @Override
    public RspLoginUser findUser() {
        try {
            Long userId = StpSysUtil.getLoginIdAsLong();

            SysUser sysUser = getById(userId);
            if (sysUser == null) {
                throw new ServiceException("用户不存在");
            }
            RspLoginUser loginUser = new RspLoginUser();

            loginUser.setId(userId);
            loginUser.setNickName(sysUser.getNickName());
            loginUser.setAvatar(sysUser.getAvatar());

            SysRole sysRole = sysRoleUserService.findRoleByUserId(userId);
            if (sysRole == null) {
                // 处理用户角色不存在的情况
                throw new ServiceException("未绑定角色");
            }
            List<SysMenu> sysMenus = sysMenuService.findPermissionByRoleId(sysRole.getId());

            String roleCode = sysRole.getCode();

            List<String> permissionList = sysMenus.stream()
                    .map(SysMenu::getPermission)
                    .collect(Collectors.toList());

            loginUser.setRole(roleCode);
            loginUser.setSysPermissions(permissionList);

            return loginUser;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public IPage<SysUser> pageList(ReqPageBean reqPageBean) {
        int pageNum = reqPageBean.getPage();
        int pageSize = reqPageBean.getSize();

        int total = count();
        List<SysUser> list = null;
        Page<SysUser> page = new Page<>(pageNum, pageSize);
        if (total > 0) {
            Map<String, Object> params = new HashMap<>();

            params.put("page", pageNum);
            params.put("size", pageSize);

            PageUtil.pageParamConver(params, true);

            list = this.baseMapper.pageList(params);

            page.setRecords(list);
            page.setTotal(total);
        }
        return page;
    }

    @Override
    public boolean updateStatus(Long id, int status) {
        QueryWrapper<SysUser> wrapper = new QueryWrapper<>();
        wrapper.eq("id", id);

        SysUser sysUser = new SysUser();
        sysUser.setId(id);
        sysUser.setStatus(status);
        return this.updateById(sysUser);
    }

    @Override
    @Transactional
    public boolean saveBean(ReqUserBean reqUserBean) {
        SysUser user = new SysUser();
        BeanUtils.copyProperties(reqUserBean, user);

        Date now = new Date();
        if (user.getId() == null || user.getId() == 0L) {
            String pwd = "123456";
            pwd = MD5Util.encode(MD5Util.md5Hex(pwd));

            user.setPassword(pwd);
            user.setCreateTime(now);
        }
        user.setUpdateTime(now);
        this.saveOrUpdate(user);

        sysRoleUserService.deleteByUserId(user.getId());
        return sysRoleUserService.saveBean(user.getId(), reqUserBean.getRoleId());
    }

    @Override
    @Transactional
    public ResultCodeEnum updatePassword(Long id, String oldPassword, String newPassword)  throws ServiceException {
        try {
            SysUser sysUser = this.getById(id);
            if (StringUtils.isNoneBlank(oldPassword)) {
                String oldPwd = MD5Util.encode(oldPassword);
                if(!StringUtils.equals(oldPwd, sysUser.getPassword())) {
                    return ResultCodeEnum.OLD_PWD_ERROR;
                }
            }

            String newPwd = MD5Util.encode(MD5Util.md5Hex(newPassword));

            SysUser user = new SysUser();
            user.setId(id);
            user.setPassword(newPwd);

            boolean isUpdate = updateById(user);

            log.info("修改密码：{}", user);
            return isUpdate ? ResultCodeEnum.SUCCESS : ResultCodeEnum.ERROR;
        } catch (Exception e) {
            throw new ServiceException(e);
        }
    }
}
