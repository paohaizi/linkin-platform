package com.link.in.sys.controller;

import cn.dev33.satoken.annotation.SaCheckPermission;
import cn.dev33.satoken.util.SaResult;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.link.in.common.enums.ResultCodeEnum;
import com.link.in.common.exception.service.ServiceException;
import com.link.in.common.util.R;
import com.link.in.sys.annotation.SaSysCheckPermission;
import com.link.in.sys.bean.ReqPageBean;
import com.link.in.sys.bean.ReqUserBean;
import com.link.in.sys.bean.RspLoginUser;
import com.link.in.sys.entity.SysUser;
import com.link.in.sys.service.SysUserService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import javax.validation.Valid;

@Slf4j
@RestController
@RequestMapping("/sys/user")
public class SysUserController {
    @Autowired
    SysUserService sysUserService;

    @GetMapping("/getInfo")
    public R getInfo() {
        RspLoginUser rspLoginUser = sysUserService.findUser();
        log.info("访问后台用户信息：{}", rspLoginUser);
        return R.data(rspLoginUser);
    }

    @PostMapping("/list")
    public R list(@Valid @RequestBody ReqPageBean reqPageBean) {
        IPage<SysUser> page = sysUserService.pageList(reqPageBean);
        return R.data(page);
    }

    @PostMapping("/updateStatus/{id}/{status}")
    public R updateStatus(@PathVariable Long id, @PathVariable int status) {
        if (status > 1 || status < 0) {
            return R.error("异常");
        }
        boolean isUpdate = sysUserService.updateStatus(id, status);
        return isUpdate ? R.success() : R.error("失败");
    }

    @PostMapping("/saveBean")
    public R saveBean(@Valid @RequestBody ReqUserBean reqUserBean) {
        boolean isSave = sysUserService.saveBean(reqUserBean);
        return isSave ? R.success() : R.error("失败");
    }

    @SaSysCheckPermission("post:user:resetPwd")
    @PostMapping(value = "/resetPwd/{id}")
    public R resetPassword(@PathVariable Long id) {
        if (id == 1L) {
            return R.error("超级管理员不给予修改");
        }
        ResultCodeEnum resultCodeEnum = sysUserService.updatePassword(id, null, "123456");
        return resultCodeEnum.getCode() == 200 ? R.success("重置密码成功") : R.error("重置失败");
    }
}
