package com.link.in.sys.mapper;

import com.link.in.sys.entity.SysRole;
import com.link.in.sys.entity.SysRoleUser;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author Ujun
 * @since 2024-03-16
 */
public interface SysRoleUserMapper extends BaseMapper<SysRoleUser> {

    SysRole findRoleByUserId(Long userId);
}
