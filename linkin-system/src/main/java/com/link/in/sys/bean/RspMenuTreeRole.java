package com.link.in.sys.bean;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.link.in.common.util.TreeEntity;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * @author Ujun
 * @date 2024/3/22
 * @apiNote
 */
@Data
public class RspMenuTreeRole implements Serializable, TreeEntity<RspMenuTreeRole, Integer> {
    private Integer id;
    private Integer parentId;
    private String path;
    private String component;
    private String label;
    private String title;
    private String name;
    private String permission;
    private String icon;
    private Integer sort;
    private int level;
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
    private Date createTime;
    private List<RspMenuTreeRole> children;
    private String titleZh;
    private String titleUs;
}
