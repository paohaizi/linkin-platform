package com.link.in.sys.mapper;

import com.link.in.sys.entity.SysMenu;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author Ujun
 * @since 2024-03-16
 */
public interface SysMenuMapper extends BaseMapper<SysMenu> {

    List<SysMenu> findByRoleId(Integer id);

    List<SysMenu> findPermissionByRoleId(Integer id);

    int deleteSubTreeById(Integer id);
}
