package com.link.in.sys.service.impl;

import cn.dev33.satoken.util.SaResult;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.link.in.common.exception.service.ServiceException;
import com.link.in.sys.bean.ReqRoleBean;
import com.link.in.sys.entity.SysRole;
import com.link.in.sys.mapper.SysRoleMapper;
import com.link.in.sys.service.SysRoleService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author Ujun
 * @since 2024-03-16
 */
@Slf4j
@Service
public class SysRoleServiceImpl extends ServiceImpl<SysRoleMapper, SysRole> implements SysRoleService {

    @Override
    public boolean saveBean(ReqRoleBean reqRoleBean) {
        SysRole sysRole = new SysRole();
        BeanUtils.copyProperties(reqRoleBean, sysRole);

        SysRole role = this.findByCode(sysRole.getCode());

        if (sysRole.getId() == null) {
            if (role != null) {
                throw new ServiceException("角色code已存在");
            }
            Date date = new Date();
            sysRole.setCreateTime(date);
        } else {
            if (!role.getId().equals(sysRole.getId())) {
                throw new ServiceException("角色code已存在");
            }
        }
        sysRole.setUpdateTime(new Date());
        return saveOrUpdate(sysRole);
    }

    @Override
    public List<SysRole> findAll(Map<String, Object> paramMap) {
        String name = MapUtils.getString(paramMap, "name");

        QueryWrapper<SysRole> wrapper = new QueryWrapper<>();
        if (StringUtils.isNotBlank(name)) {
            wrapper.eq("name", name);
        }
        return list(wrapper);
    }

    private SysRole findByCode(String code) {
        QueryWrapper<SysRole> wrapper = new QueryWrapper<>();
        wrapper.eq("code", code);
        return getOne(wrapper);
    }
}
