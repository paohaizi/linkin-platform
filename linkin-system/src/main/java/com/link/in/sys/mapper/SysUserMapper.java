package com.link.in.sys.mapper;

import com.link.in.sys.entity.SysUser;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

import java.util.List;
import java.util.Map;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author Ujun
 * @since 2024-03-16
 */
public interface SysUserMapper extends BaseMapper<SysUser> {

    List<SysUser> pageList(Map<String, Object> params);
}
