package com.link.in.common.util;

import com.link.in.common.enums.ResultCodeEnum;
import lombok.Data;

import java.io.Serializable;

/**
 * @author Ujun
 * @date 2024/3/22
 * @apiNote
 */
public class R implements Serializable {

    private static final long serialVersionUID = 7498483649536881777L;

    private Integer code;

    private String msg;

    private Object data;

    public R() {
    }

    public R(Integer code, String msg, Object data) {
        this.code = code;
        this.msg = msg;
        this.data = data;
    }

    public static R success() {
        return new R(ResultCodeEnum.SUCCESS.getCode(), ResultCodeEnum.SUCCESS.getMsg(), null);
    }

    public static R success(String msg) {
        return new R(ResultCodeEnum.SUCCESS.getCode(), msg, null);
    }

    /*public static R success(Object data) {
        return new R(ResponseCode.SUCCESS.getCode(), null, data);
    }*/

    public static R success(String msg, Object data) {
        return new R(ResultCodeEnum.SUCCESS.getCode(), msg, data);
    }

    public static R error(String msg) {
        return new R(ResultCodeEnum.ERROR.getCode(), msg, null);
    }

    /*public static R error(Object data) {
        return new R(ResponseCode.ERROR.getCode(), null, data);
    }*/

    public static R error(String msg, Object data) {
        return new R(ResultCodeEnum.ERROR.getCode(), msg, data);
    }

    public static R error(ResultCodeEnum responseCode) {
        return new R(responseCode.getCode(), responseCode.getMsg(), null);
    }

    public static R error(Integer code, String msg, Object data) {
        return new R(code, msg, data);
    }

    public static R data(Object data) {
        return new R(ResultCodeEnum.SUCCESS.getCode(), ResultCodeEnum.SUCCESS.getMsg(), data);
    }

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public Object getData() {
        return data;
    }

    public void setData(Object data) {
        this.data = data;
    }
}
