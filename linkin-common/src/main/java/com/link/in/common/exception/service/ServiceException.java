package com.link.in.common.exception.service;

import com.link.in.common.exception.BaseException;

/**
 * @author Ujun
 * @date 2024/3/16
 * @apiNote
 */
public class ServiceException extends BaseException {

    /**
     *
     */
    private static final long serialVersionUID = -2437160791033393978L;

    public ServiceException(String msg) {
        super(msg);
    }

    public ServiceException(Exception e){
        this(e.getMessage());
    }
}
