package com.link.in.common.exception.service;

import lombok.Data;

/**
 * @author Ujun
 * @date 2024/3/29
 * @apiNote
 */
@Data
public class RateLimiterException extends RuntimeException {
    private int code;
    private String message;

    public RateLimiterException(int code, String message) {
        this.code = code;
        this.message = message;
    }
}
