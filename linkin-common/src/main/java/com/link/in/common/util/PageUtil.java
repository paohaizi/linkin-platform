package com.link.in.common.util;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections.MapUtils;

import java.util.Map;

/**
 * @author Ujun
 * @date 2024/3/23
 * @apiNote
 */
@Slf4j
public class PageUtil {
    /**
     * 分页参数，起始位置，从0开始
     */
    public static final String PAGE = "page";
    /**
     * 分页参数，每页数据条数
     */
    public static final String SIZE = "size";

    /**
     * 转换并校验分页参数<br>
     * mybatis中limit #{start, JdbcType=INTEGER}, #{length,
     * JdbcType=INTEGER}里的类型转换貌似失效<br>
     * 我们这里先把他转成Integer的类型
     *
     * @param params
     * @param required
     * 分页参数是否是必填
     */
    public static void pageParamConver(Map<String, Object> params, boolean required) {
        if (required) {// 分页参数必填时，校验参数
            if (params == null || !params.containsKey(PAGE) || !params.containsKey(SIZE)) {
                throw new IllegalArgumentException("请检查分页参数," + PAGE + "," + SIZE);
            }
        }
        boolean paramsNoEmpty = params != null && !params.isEmpty();
        if (paramsNoEmpty) {
            if (params.containsKey(PAGE)) {
                Integer start = MapUtils.getInteger(params, PAGE);
                Integer length = MapUtils.getInteger(params, SIZE);
                if (start < 0) {
                    log.error("page：{}，重置为0", start);
                    start = 0;
                }
                params.put(PAGE, (start-1)*length);
            }

            if (params.containsKey(SIZE)) {
                Integer length = MapUtils.getInteger(params, SIZE);
                if (length < 0) {
                    log.error("size：{}，重置为0", length);
                    length = 0;
                }
                params.put(SIZE, length);
            }
        }
    }
}
