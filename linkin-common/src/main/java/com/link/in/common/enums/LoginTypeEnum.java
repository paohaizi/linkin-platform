package com.link.in.common.enums;

import lombok.Getter;

/**
 * @author Ujun
 * @date 2024/3/15
 * @apiNote
 */
@Getter
public enum LoginTypeEnum {
    manager_bs("manager_bs", "pc后台端登录"),
    user_cs("user_cs", "c端用户登录"),
    ;

    private String code;

    private String msg;

    LoginTypeEnum(String code, String msg) {
        this.code = code;
        this.msg = msg;
    }
}
