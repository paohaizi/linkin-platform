package com.link.in.common.util;

import java.util.List;

/**
 * @author Ujun
 * @date 2024/3/22
 * @apiNote
 */
public interface TreeEntity<E, T> {
    public T getId();

    public T getParentId();

    public void setChildren(List<E> childList);
}
