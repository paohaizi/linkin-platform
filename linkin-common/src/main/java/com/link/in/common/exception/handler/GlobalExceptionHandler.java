package com.link.in.common.exception.handler;

import cn.dev33.satoken.exception.NotLoginException;
import cn.dev33.satoken.exception.NotPermissionException;
import cn.dev33.satoken.exception.NotRoleException;
import cn.dev33.satoken.util.SaResult;
import com.link.in.common.exception.BaseException;
import com.link.in.common.exception.service.ServiceException;
import com.link.in.common.util.R;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import javax.servlet.http.HttpServletRequest;

@Slf4j
@ControllerAdvice
public class GlobalExceptionHandler {
    @ResponseBody
    @ExceptionHandler
    public R exceptionHandler(HttpServletRequest req, Exception e) {
        if (e instanceof NotPermissionException) {
            return R.error(403, "您缺少该功能的操作权限", null);
        }
        if (e instanceof NotRoleException) {
            return R.error(402, "您的角色不支持操作该功能", null);
        }
        if (e instanceof NotLoginException) {
            return R.error(401, "您的登录凭证已过期", null);
        }
        if (e instanceof BaseException) {
            return R.error(1000, e.getMessage(), null);
        }
        if (e instanceof MethodArgumentNotValidException) {
            MethodArgumentNotValidException e1 = (MethodArgumentNotValidException) e;
            return R.error(1000, e1.getBindingResult().getAllErrors().get(0).getDefaultMessage(), null);
        }
        log.error("方法:" + req.getMethod() + "," + req.getServletPath() + "," + e.getMessage(), e);
        return R.error("异常");
    }
}
