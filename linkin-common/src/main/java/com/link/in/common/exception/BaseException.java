package com.link.in.common.exception;

import com.link.in.common.enums.ResultCodeEnum;

public class BaseException extends RuntimeException {
    /**
     *
     */
    private static final long serialVersionUID = 7859712770754900356L;

    private Integer code = -1;

    public Integer getCode() {
        return code;
    }

    public BaseException(String msg) {
        super(msg);
    }

    public BaseException(ResultCodeEnum errEnum) {
        super(errEnum.getMsg());
        this.code = errEnum.getCode();
    }

    public BaseException(Exception e) {
        this(e.getMessage());
    }
}
