package com.link.in.common.util;

import org.apache.commons.lang3.StringUtils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author Ujun
 * @date 2024/3/22
 * @apiNote
 */
public class TreeUtil {
    public static <T extends TreeEntity> List<T> buildTreeData(List<T> srcList) {
        List<T> desList = new ArrayList<>();
        Map<String, List<T>> map = new HashMap<String, List<T>>();
        List<T> l = null;
        for (T obj : srcList) {
            String pId = obj.getParentId() + "";
            String key = StringUtils.isBlank(pId) ? "-1" : pId;
            if (map.get(key) == null) {
                l = new ArrayList<T>();
            } else {
                l = map.get(key);
            }
            l.add(obj);
            map.put(key, l);
        }
        for (T obj2 : map.get("-1")) {
            if (obj2 == null) {
                return null;
            }
            String id = obj2.getId() + "";
            obj2.setChildren(getChild(map, id));
            desList.add(obj2);
        }
        return desList;
    }

    public static <T extends TreeEntity> List getChild(Map<String, List<T>> map, String i) {
        if (map.get(i) == null) {
            return null;
        }
        List<T> childs = map.get(i);//取到子节点
        try {
            for (T obj : childs) {
                String id = obj.getId() + "";
                obj.setChildren(getChild(map, id));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return childs;
    }
}
