package com.link.in.common.enums;

public enum ResultCodeEnum {
    // 系统模块
    SUCCESS(200, "操作成功"),
    ERROR(400, "操作失败"),
    SERVER_ERROR(500, "服务器异常"),
    PARAMS_ERROR(501, "参数异常, 请检查"),
    UPDATE_ERROR(502, "修改失败, 请重试"),
    OLD_PWD_ERROR(503, "旧密码错误")

    ;
    private int code;

    private String msg;

    public int getCode() {
        return this.code;
    }

    public String getMsg() {
        return this.msg;
    }

    ResultCodeEnum(int code, String msg) {
        this.code = code;
        this.msg = msg;
    }
}
