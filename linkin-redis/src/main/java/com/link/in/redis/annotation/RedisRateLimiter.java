package com.link.in.redis.annotation;

import java.lang.annotation.*;

/**
 * @author Ujun
 * @date 2024/3/29
 * @apiNote
 */
@Documented
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.METHOD, ElementType.TYPE})
public @interface RedisRateLimiter {
    /**
     * 允许访问的次数，默认值20
     */
    int count() default 20;

    /**
     * 时间段，单位为秒，默认值一分钟
     */
    int time() default 60;
}