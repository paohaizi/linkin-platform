package com.link.in.redis.handle;

/**
 * @author Ujun
 * @date 2024/3/29
 * @apiNote
 */
public interface RedisDelayQueueHandle<T> {
    void execute(T t);
}

