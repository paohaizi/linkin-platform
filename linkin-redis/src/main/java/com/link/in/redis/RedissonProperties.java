package com.link.in.redis;

import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * @author Ujun
 * @date 2024/3/29
 * @apiNote
 */
@ConfigurationProperties(prefix = "spring.redis.redisson")
public class RedissonProperties {

    private String config;
    private String enable  ;

    public String getConfig() {
        return config;
    }

    public void setConfig(String config) {
        this.config = config;
    }

    public String getEnable() {
        return enable;
    }

    public void setEnable(String enable) {
        this.enable = enable;
    }
}