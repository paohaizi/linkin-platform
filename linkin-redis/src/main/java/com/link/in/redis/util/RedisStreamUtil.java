package com.link.in.redis.util;

import io.lettuce.core.models.stream.PendingMessage;
import io.lettuce.core.models.stream.PendingMessages;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Range;
import org.springframework.data.redis.connection.stream.*;
import org.springframework.data.redis.connection.stream.Record;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Component;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * @author Ujun
 * @date 2024/3/29
 * @apiNote
 */
@Component
public class RedisStreamUtil {
    @Autowired
    private StringRedisTemplate stringRedisTemplate;

    /**
     * 创建消费者组
     *
     * @param key   关键
     * @param group 集团
     * @return {@link String}
     */
    public String createGroup(String key, String group) {
        return stringRedisTemplate.opsForStream().createGroup(key, group);
    }

    /**
     * 从队列中读取指定范围消息
     *
     * @param key       关键
     * @param recordIds 记录id
     * @return {@link List}<{@link ObjectRecord}<{@link String}, {@link String}>>
     */
    public List<ObjectRecord<String, String>> range(String key, Set<RecordId> recordIds) {
        if (recordIds == null || recordIds.isEmpty()) {
            return Collections.emptyList();
        }
        // 消息id排序
        List<String> sortedMessageIds = recordIds.stream().map(RecordId::getValue)
                .sorted(Comparator.comparingLong(messageId -> Long.parseLong(messageId.split("-")[0])))
                .sorted(Comparator.comparingInt(messageId -> Integer.parseInt(messageId.split("-")[1])))
                .collect(Collectors.toList());

        // 消息范围 闭区间
        Range<String> range = Range.closed(sortedMessageIds.get(0), sortedMessageIds.get(sortedMessageIds.size() - 1));
        return stringRedisTemplate.opsForStream().range(String.class, key, range);
    }

    /**
     * 确认已消费
     *
     * @param key       关键
     * @param group     集团
     * @param recordIds 记录id
     * @return {@link Long}
     */
    public Long ack(String key, String group, String... recordIds) {
        return stringRedisTemplate.opsForStream().acknowledge(key, group, recordIds);
    }

    /**
     * 发送消息
     *
     * @param record 记录
     * @return {@link String}
     */
    public String add(Record record) {
        return stringRedisTemplate.opsForStream().add(record).getValue();
    }

    /**
     * 删除消息，这里的删除仅仅是设置了标志位，不影响消息总长度
     * 消息存储在stream的节点下，删除时仅对消息做删除标记，当一个节点下的所有条目都被标记为删除时，销毁节点
     *
     * @param key       关键
     * @param recordIds 记录id
     * @return {@link Long}
     */
    public Long del(String key, String... recordIds) {
        return stringRedisTemplate.opsForStream().delete(key, recordIds);
    }
}
