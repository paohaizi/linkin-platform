package com.link.in.redis.util;

/**
 * @author Ujun
 * @date 2024/3/29
 * @apiNote
 */
public interface RedisSubscribeCallback {
    void callback(String msg);
}
