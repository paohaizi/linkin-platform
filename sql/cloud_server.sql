/*
Navicat MySQL Data Transfer

Source Server         : localhost
Source Server Version : 80034
Source Host           : localhost:3306
Source Database       : cloud_server

Target Server Type    : MYSQL
Target Server Version : 80034
File Encoding         : 65001

Date: 2024-03-29 23:19:22
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for sys_menu
-- ----------------------------
DROP TABLE IF EXISTS `sys_menu`;
CREATE TABLE `sys_menu` (
  `id` int NOT NULL AUTO_INCREMENT,
  `parent_Id` int NOT NULL,
  `name` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '标识名称',
  `path` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL,
  `component` varchar(250) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL COMMENT '组件',
  `redirect` varchar(50) DEFAULT NULL,
  `title_zh` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL COMMENT '显示标题',
  `title_us` varchar(50) DEFAULT NULL,
  `icon` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL COMMENT '图标',
  `level` int DEFAULT NULL COMMENT '0目录 1菜单 2按钮',
  `sort` int NOT NULL COMMENT '排序',
  `create_Time` datetime NOT NULL,
  `update_Time` datetime NOT NULL,
  `is_menu` int DEFAULT NULL COMMENT '是否菜单 1 是 2 不是',
  `permission` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL COMMENT '按钮权限',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=54 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- ----------------------------
-- Records of sys_menu
-- ----------------------------
INSERT INTO `sys_menu` VALUES ('1', '-1', 'SysManager', '/system', 'LAYOUT', null, '后台管理', 'sys_manager', 'gear', '0', '2', '2019-09-05 11:37:02', '2023-09-21 15:55:38', '1', null);
INSERT INTO `sys_menu` VALUES ('2', '1', '用户管理', 'user/index', 'system/user/index', null, '用户管理', 'user_manager', 'user', '1', '2', '2017-11-17 16:56:59', '2019-09-05 11:37:26', '1', null);
INSERT INTO `sys_menu` VALUES ('3', '1', '角色管理', '/role/index', 'system/role/index', null, '角色管理', 'role_manager', 'user-tie', '1', '3', '2017-11-17 16:56:59', '2019-09-05 11:37:38', '1', null);
INSERT INTO `sys_menu` VALUES ('4', '1', '菜单管理', '/menu/index', 'system/menu/index', null, '菜单管理', 'menu_manager', 'gauge', '1', '4', '2017-11-17 16:56:59', '2019-09-05 11:37:57', '1', null);
INSERT INTO `sys_menu` VALUES ('7', '4', '菜单树形数据', null, null, null, null, null, null, '2', '4', '2023-09-21 12:21:53', '2023-09-21 12:21:53', '0', 'get:menu:treeMenu');
INSERT INTO `sys_menu` VALUES ('8', '4', '菜单分配角色', null, null, null, null, null, null, '2', '4', '2023-09-21 15:03:09', '2023-09-21 15:03:11', '0', 'post:menu:granted');
INSERT INTO `sys_menu` VALUES ('9', '4', '菜单编辑', null, null, null, null, null, null, '2', '4', '2023-09-21 15:03:44', '2023-09-21 15:03:52', '0', 'post:menu:saveOrUpdate');
INSERT INTO `sys_menu` VALUES ('10', '4', '菜单删除', null, null, null, null, null, null, '2', '4', '2023-09-21 15:04:28', '2023-09-21 15:04:31', '0', 'delete:menu:delete');
INSERT INTO `sys_menu` VALUES ('11', '3', '角色分页查询', null, '', null, null, null, null, '2', '3', '2023-09-21 15:05:56', '2023-09-21 15:05:59', '0', 'get:role:pageList');
INSERT INTO `sys_menu` VALUES ('12', '3', '角色编辑', null, null, null, null, null, null, '2', '3', '2023-09-21 15:06:33', '2023-09-21 15:06:36', '0', 'post:role:saveOrUpdate');
INSERT INTO `sys_menu` VALUES ('13', '2', '用户分页查询', null, null, null, null, null, null, '2', '2', '2023-09-21 15:37:03', '2023-09-21 15:37:05', '0', 'get:user:pageList');
INSERT INTO `sys_menu` VALUES ('14', '2', '用户编辑', null, null, null, null, null, null, '2', '2', '2023-09-21 15:37:44', '2023-09-21 15:37:47', '0', 'post:user:saveOrUpdate');
INSERT INTO `sys_menu` VALUES ('49', '2', '用户重置密码', '', '', null, null, null, '', '2', '2', '2024-03-24 01:55:21', '2024-03-24 01:55:21', null, 'post:user:resetPwd');

-- ----------------------------
-- Table structure for sys_role
-- ----------------------------
DROP TABLE IF EXISTS `sys_role`;
CREATE TABLE `sys_role` (
  `id` int NOT NULL AUTO_INCREMENT,
  `code` varchar(32) NOT NULL COMMENT '角色code',
  `name` varchar(50) NOT NULL COMMENT '角色名',
  `create_Time` datetime NOT NULL,
  `update_Time` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `code` (`code`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- ----------------------------
-- Records of sys_role
-- ----------------------------
INSERT INTO `sys_role` VALUES ('1', 'ADMIN', '超级管理员', '2021-04-01 16:56:59', '2021-10-17 21:20:33');
INSERT INTO `sys_role` VALUES ('5', 'manager', '管理员', '2024-03-23 22:55:12', '2024-03-23 22:55:12');

-- ----------------------------
-- Table structure for sys_role_menu
-- ----------------------------
DROP TABLE IF EXISTS `sys_role_menu`;
CREATE TABLE `sys_role_menu` (
  `role_Id` int NOT NULL,
  `menu_Id` int NOT NULL,
  PRIMARY KEY (`role_Id`,`menu_Id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- ----------------------------
-- Records of sys_role_menu
-- ----------------------------
INSERT INTO `sys_role_menu` VALUES ('1', '1');
INSERT INTO `sys_role_menu` VALUES ('1', '2');
INSERT INTO `sys_role_menu` VALUES ('1', '3');
INSERT INTO `sys_role_menu` VALUES ('1', '4');
INSERT INTO `sys_role_menu` VALUES ('1', '7');
INSERT INTO `sys_role_menu` VALUES ('1', '8');
INSERT INTO `sys_role_menu` VALUES ('1', '9');
INSERT INTO `sys_role_menu` VALUES ('1', '10');
INSERT INTO `sys_role_menu` VALUES ('1', '11');
INSERT INTO `sys_role_menu` VALUES ('1', '12');
INSERT INTO `sys_role_menu` VALUES ('1', '13');
INSERT INTO `sys_role_menu` VALUES ('1', '14');
INSERT INTO `sys_role_menu` VALUES ('5', '1');
INSERT INTO `sys_role_menu` VALUES ('5', '3');
INSERT INTO `sys_role_menu` VALUES ('5', '4');
INSERT INTO `sys_role_menu` VALUES ('5', '7');
INSERT INTO `sys_role_menu` VALUES ('5', '8');
INSERT INTO `sys_role_menu` VALUES ('5', '9');
INSERT INTO `sys_role_menu` VALUES ('5', '10');
INSERT INTO `sys_role_menu` VALUES ('5', '11');
INSERT INTO `sys_role_menu` VALUES ('5', '12');

-- ----------------------------
-- Table structure for sys_role_user
-- ----------------------------
DROP TABLE IF EXISTS `sys_role_user`;
CREATE TABLE `sys_role_user` (
  `user_Id` bigint NOT NULL,
  `role_Id` int NOT NULL,
  PRIMARY KEY (`user_Id`,`role_Id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- ----------------------------
-- Records of sys_role_user
-- ----------------------------
INSERT INTO `sys_role_user` VALUES ('1', '1');
INSERT INTO `sys_role_user` VALUES ('1447495048226914308', '1');

-- ----------------------------
-- Table structure for sys_user
-- ----------------------------
DROP TABLE IF EXISTS `sys_user`;
CREATE TABLE `sys_user` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `username` varchar(50) NOT NULL,
  `password` varchar(60) NOT NULL,
  `nick_name` varchar(255) DEFAULT NULL,
  `avatar` varchar(1024) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL,
  `phone` varchar(11) DEFAULT NULL,
  `email` varchar(25) DEFAULT NULL,
  `sex` tinyint(1) DEFAULT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `create_time` datetime NOT NULL,
  `update_time` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `username` (`username`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=1447495048226914309 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- ----------------------------
-- Records of sys_user
-- ----------------------------
INSERT INTO `sys_user` VALUES ('1', 'admin', 'c7340c14267618a844584d4fe5fe4ad6ab20b40312855c1e', '管理员', null, '18682065963', null, '1', '1', '2021-04-01 16:56:59', '2021-10-17 21:17:09');
INSERT INTO `sys_user` VALUES ('1447495048226914308', 'faker', 'c1c21728199a60ed7070f636d2631a74450270869b30b65f', 'faker1', null, '', '', null, '1', '2024-03-23 18:46:04', '2024-03-24 11:44:52');
